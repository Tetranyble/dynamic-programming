const fibb = ( n ) => {
    if(n <= 2) return 1;
    return fib(n-1) + fib(n-2);
   
}

const fib = ( n, memo = {}) => {
    if(n in memo) return memo[n];
    if(n <= 2) return 1;
    memo[n] = fib(n-1, memo) + fib(n-2, memo);
    return memo[n];
}



/**
 * this implemetation is very slow
 * @param {*} m 
 * @param {*} n 
 * @returns 
 */
const gridTraveller = (m,n) => {
    if (m === 1 && n == 1) return 1;
    if(m === 0  || n === 0) return 0;
    return gridTraveller(m-1,n) + gridTraveller(m,n-1);
}

/**
 * improved implementation
 * @param {*} m 
 * @param {*} n 
 * @returns 
 */
 const gridTraveller = (m,n) => {
     const key = m + ',' + n;
    if (key in memo) return memo[key];
    if (m === 1 && n == 1) return 1;
    if(m === 0  || n === 0) return 0;
    memo[key] = gridTraveller(m-1,n, memo) + gridTraveller(m,n-1, memo);
    return memo[key];
}

//console.log(fibb(900))
console.log(gridTraveller(18,19))