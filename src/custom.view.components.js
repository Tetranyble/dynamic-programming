Vue.component('coupon', {
    props: ['code'],

    template: `
        <input type="text"
               :value="code"
               @input="updateCode($event.target.value)"
               ref="input"> /** a quick identifier*/
    `,

    methods: {
        updateCode(code) {
            // Atttach validation + sanitization here.
            if(code === 'FREE'){
                alert('this coupon is no longer valid. sorry!')
                
                //this line clears out the input referenced meanwhile a local copy of the input
                //is contained in CODE
                this.$refs.input.value = code = '';
            }

            this.$emit('input', code);
        }
    }
});


new Vue({
    el: '#app',

    data: {
        coupon: 'FREEBIE' // Maybe from DB or querystring.
    }
});

//the coupon component can be instantiated as
// <coupon v-model=coupon></coupon>